l9_ = float(input('長さ[cm] = '))
l9 = l9_ * 0.01
E9_ = float(input('電極間の電場10^2[V/m] = '))
E9 = E9_ * 100
B9_ = float(input('垂直の磁場10^-4[T] = '))
B9 = B9_ * 10**(-4)
v9 = E9 / B9
t9_ = l9 / v9 # [s]
t9 = t9_ * 1000000000 # [ns]

print("時間t = ", t9, "[ns]")