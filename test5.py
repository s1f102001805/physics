import math

r1_5 = float(input('半径1[cm] = '))
r2_5 = float(input('半径2[cm] = '))
h5 = float(input('高さ[cm ] = '))
E0 = 8.85 * 10**(-12)
log16 = 0.47
r5 = r2_5 / r1_5
C5 = (2 * 3.14 * E0 * h5) / (math.log(r5))

print(C5)