R11_ = float(input('抵抗値[μΩ] = '))
R11 = R11_ * 10**(-6)
l11_ = float(input('長さ[cm] = '))
l11 = l11_ * 0.01
B11 = float(input('コイルの大きさ[T] = '))
v11_ = float(input('速度[cm/s] = '))
v11 = v11_ * 0.01
F11 = B11 * B11 * l11 * l11 * v11 / R11

print("力F = ", F11, "[N]")