r112 = float(input('内半径[mm] = '))
r212 = float(input('外半径[mm] = '))
N12 = float(input('巻きつけた導線[回] = '))
I12 = float(input('電流[A] = '))
r12 = r212 - (r212 - r112) / 2
u12 = 6.3 * 10**-3
B12 = (u12 * N12 * I12) / (2 * 3.14 * r12 * 0.001)

print('磁場B = ', B12, 'T')