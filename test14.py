print("問14")
a14 = float(input("歯の枚数[枚] = "))
l14_ = float(input("隙間を通った光の距離[km] = "))
l14 = l14_ * 1000
print("光速度[m/s]")
c14 = 3 * 10**8
dt14 = (2 * l14) / c14
t14 = 2 * a14 * dt14
f14 = 1 / t14

print("問14 回転数f = ", f14, "[s^-1]")