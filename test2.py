m2_g = float(input('質量[g] = '))
m2 = m2_g / 1000
q2_ = float(input('電荷(10^-5)[C] = '))
q2 = q2_ * 0.00001 # 10^-5に直す
F2_ = float(input('大きさ(10^4)[N] = '))
F2 = F2_ * 10000 # 10^4になおす
d2 = float(input('距離[m] = '))
N2 = q2 * F2 # 球が電場から受ける力
E2 = N2 * d2 # 金属玉が得るエネルギーE

v2 = (2 * E2 / m2)**(1/2)

print("速度Vr = ", v2, "[m/s]")