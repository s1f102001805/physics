import math

d8_ = int((input('長さ[cm] = ')))
d8 = d8_ / 100
f8 = int((input('磁荷の強さ[N/T] = ')))
theta8_ = int(input('北からずらした角度[°] = '))
theta8 = math.radians(theta8_)
B = 30 * 10**(-6)
F8 = f8 * B
N8 = F8 * d8 * math.sin(theta8)

print("トルク = ", N8 * 10**6, "[μJ]]")