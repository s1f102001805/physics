import math
# float(input())

print('問1')
T1 = float(input('長さ[m] ＝ '))
halfT1 = T1 / 2
m1 = float(input('質量[kg] = '))
g = 9.8
k = 9 #10^9は放置
r1_ = float(input('金属間の距離[cm] = '))
r1 = r1_ / 100 #メートルに変換
halfr1 = r1/2
h1 = (halfT1**2 - halfr1**2)**(1/2)
tan1 = halfr1 / h1
q1_2 = tan1 * m1 * g * r1 * r1 / k
q1_3 = q1_2 * 1000
q1 = q1_3**(1/2)

print("問1 電荷q = ", q1, "[μC]")

# 問2
print('問2')
m2_g = float(input('質量[g] = '))
m2 = m2_g / 1000
q2_ = float(input('電荷(10^-5)[C] = '))
q2 = q2_ * 0.00001 # 10^-5に直す
F2_ = float(input('大きさ(10^4)[N] = '))
F2 = F2_ * 10000 # 10^4になおす
d2 = float(input('距離[m] = '))
N2 = q2 * F2 # 球が電場から受ける力
E2 = N2 * d2 # 金属玉が得るエネルギーE

v2 = (2 * E2 / m2)**(1/2)

print("問2 速度Vr = ", v2, "[m/s]")

# 問3
print('問3')
print('問3 電場の大きさ E0 = 97 [N/C]')

# 4
print('問4')
e = 1.6 * 10**(-19)
u = 1.66 * 10**(-27)
e0 = 8.85 * 10**(-12)

e4_ = float(input('原子核の電荷[e] = '))
e4 = e4_ * e
e4_2_ = float(input('アルファ粒子の電荷[e] = '))
e4_2 = e4_2_ * e
u4_ = float(input('アルファ粒子の質量[u] = '))
u4 = u4_ * u
V4 = float(input('初速度(10^7)[m/s]'))

r04_ = 79 * e * e / (3.14 * e0 * u4 * V4 * V4)
r04 = r04_ * 10
print("問4 距離r0 = ", r04, "[fm]")

#　問5
print('問5')
r1_5 = float(input('半径1[cm] = '))
r2_5 = float(input('半径2[cm] = '))
h5 = float(input('高さ[cm ] = '))
E0 = 8.85 * 10**(-12)
log16 = 0.47
r5 = r2_5 / r1_5
C5 = 2 * 3.14 * E0 * h5 / (math.log(r5))

print('問5 静電気容量 C = ', C5, '[pF]')

# 問6
print('問6')
print('問6 時間 t = 40 [min]')

# 問7
print('問7')
d7 = float(input('長さ[cm] = '))
e1_7 = float(input('電圧[V] = '))
I7 = float(input('電流[A] = '))
e2_7 = float(input('起電力[V] = '))
x7 = e2_7 * d7 / e1_7

print("問7 距離 x = ", x7, "[cm]")

# 問8
print('問8')
d8_ = int((input('長さ[cm] = ')))
d8 = d8_ / 100
f8 = int((input('磁荷の強さ[N/T] = ')))
theta8_ = int(input('北からずらした角度[°] = '))
theta8 = math.radians(theta8_)
B = 30 * 10**(-6)
F8 = f8 * B
N8 = F8 * d8 * math.sin(theta8)

print("問8 トルクN = ", N8 * 10**6, "[μJ]")

# 問9
print('問9')
l9_ = float(input('長さ[cm] = '))
l9 = l9_ * 0.01
E9_ = float(input('電極間の電場10^2[V/m] = '))
E9 = E9_ * 100
B9_ = float(input('垂直の磁場10^-4[T] = '))
B9 = B9_ * 10**(-4)
v9 = E9 / B9
t9_ = l9 / v9 # [s]
t9 = t9_ * 1000000000 # [ns]

print("問9 時間t = ", t9, "[ns]")

# 問10
print('問10')
l10_ = float(input('距離[cm] = '))
l10 = l10_ * 0.01
I10 = float(input('電流[A] = '))
u010 = 1.26 * 10**(-6)
B10_ = 2 * u010 * I10 / (3.14 * l10)
B10 = B10_ * 10**6

print("問10 磁場B = ", B10, "[μT]")

# 問11
print('問11')
R11_ = float(input('抵抗値[μΩ] = '))
R11 = R11_ * 10**(-6)
l11_ = float(input('長さ[cm] = '))
l11 = l11_ * 0.01
B11 = float(input('コイルの大きさ[T] = '))
v11_ = float(input('速度[cm/s] = '))
v11 = v11_ * 0.01
F11 = B11 * B11 * l11 * l11 * v11 / R11

print("問11 力F = ", F11, "[N]")

# 問12
print('問12')
r112 = float(input('内半径[mm] = '))
r212 = float(input('外半径[mm] = '))
N12 = float(input('巻きつけた導線[回] = '))
I12 = float(input('電流[A] = '))
r12 = r212 - (r212 - r112) / 2
u12 = 6.3 * 10**-3
B12 = (u12 * N12 * I12) / (2 * 3.14 * r12 * 0.001)

print('問12 磁場B = ', B12, '[T]')